# RUN

- npm install
- npm start

# How to build

Build work very well, just need to read logs to understand why it's not working.
Astro is a static site generator, so you can build your site by running:

```bash
npm run build
```

But, you need to have your backend running with some blogs, it's can't build if you don't run pocketbase and if you didn't take time to create "posts" collection on pocketbase. If you don't, astro will not be able to generate the static site.

need:

- author : text
- title : text
- body : rich text

don't forget to open get endpoint on pocketbase to allow astro to fetch data.

URL is defined in .env

So you have to modify the .env file to match your backend URL.

If you don't "ca build pas" will not be a valid message
